import React, {useState} from 'react'
import { Typography  } from '@material-ui/core';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";   
import useLocalStorage from 'use-local-storage';
import ContactList from '../ContactList';      
import Add from '../Add';      
import { UserContext, context, IContact } from '../../Store';

const Layout = () => {
    const [ contactList, setContactList ] = useState<IContact[]>(context);
    if (localStorage.getItem("contactList") === null) {
        localStorage.setItem("contactList", JSON.stringify(context));
    }

    return (
        <div>
            <Router>
            <Typography variant="h3" align='center'>
                Contact App
            </Typography>
                <UserContext.Provider value={{contactList, setContactList}}>
                    <Switch>
                        <Route exact path="/">
                            <Redirect to="/contact"/>
                        </Route>  
                        <Route path = "/contact">
                            <ContactList/>
                        </Route>
                        <Route path = "/add">
                            <Add/>
                        </Route>
                    </Switch>
                </UserContext.Provider>        
            </Router>
        </div>
    )
}

export default Layout;
