import React, {useState, useContext, FormEvent, ChangeEvent} from 'react'
import { useHistory } from "react-router-dom";   
import { Container, Button, FormControl, Input, InputLabel, FormHelperText } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { UserContext, IContact } from '../../Store';
import './style.scss'

const Add = () => {
    let localStorageString:any = localStorage.getItem("contactList")
    let localStorageList = JSON.parse(localStorageString)
    const [term, setTerm] = useState('')
    const [number, setNumber] = useState(0)
    const history = useHistory();
    const { contactList, setContactList } = useContext(UserContext);
    const changePath = (path:string) => {
        history.push(path)
    }
    const handleName = (e:ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {        
        setTerm(e.target.value)
    }
    const handleNumber = (e:ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {        
        setNumber(parseInt(e.target.value))
    }
    const addContact = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        let date = new Date();
        const newContact:IContact = {
            id: contactList.length + 1,
            name: term,
            number: number,
            createdDate: ` ${`${date.getHours()}`.length > 1 ? date.getHours(): `0${date.getHours()}`}:${`${date.getMinutes()}`.length > 1 ? date.getMinutes():`0${date.getMinutes()}`}`
        }
        setContactList(localStorageList.concat(newContact));
        localStorage.setItem("contactList", JSON.stringify(localStorageList.concat(newContact)));
        changePath('/contact')
    }
    return (
        <div>
            <Container>
                <Button onClick={() => changePath('/contact')} color="secondary" variant="contained">
                    <ArrowBackIcon/>
                    Go Back Contact List
                </Button>
                <form className='add-form' onSubmit={(e) => addContact(e)}>
                    <FormControl>
                        <InputLabel htmlFor="my-input">Name</InputLabel>
                        <Input placeholder='Enter Name' value={term} onChange={(e) => handleName(e)} id="my-input" aria-describedby="my-helper-text" />
                    </FormControl>
                    <FormControl>
                        <InputLabel htmlFor="my-input">Number</InputLabel>
                        <Input value={number} onChange={(e) => handleNumber(e)} id="my-input" type='number' aria-describedby="my-helper-text" />
                        <FormHelperText id="my-helper-text">Number must be min 9 char</FormHelperText>
                    </FormControl>
                    <Button disabled={term.length>0 && number>100000000 ? false : true} 
                            type='submit' 
                            color="primary" 
                            variant="contained">
                            Add
                    </Button>
                </form>
            </Container>            
        </div>
    )
}

export default Add;