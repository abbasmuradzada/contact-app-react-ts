import React, {FormEvent, useContext, useEffect, useRef} from 'react';
import { UserContext, IContact } from '../../Store';
import { Container, Button, Box } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { useHistory } from "react-router-dom"; 
import './style.scss';
// import useLocalStorage from 'use-local-storage';


export default function ContactList() {
    let localStorageString:any = localStorage.getItem("contactList")
    let localStorageList = JSON.parse(localStorageString)
    
    let editIndex: Number;
    const { contactList, setContactList } = useContext(UserContext);
    const history = useHistory();
    const goToAdd = () => {
        history.push('/add')
    }

    const editContact = (index:number) => {   
        refForm.current.style.display = 'block';
        editIndex = index     
        localStorageList.map((element:IContact, index1:number) => {
            if(index1 === index){
                refNameInput.current.value = element.name;
                refNumberInput.current.value = element.number;
            }
        }) 
    }

    const onEditSubmit = (e:FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        let date = new Date();
        const editedContact = localStorageList.map((element:IContact, index1:number) => {
            if(index1 === editIndex){
                element.name = refNameInput.current.value;
                element.number = refNumberInput.current.value;
                element.createdDate= ` ${`${date.getHours()}`.length > 1 ? date.getHours(): `0${date.getHours()}`}:${`${date.getMinutes()}`.length > 1 ? date.getMinutes():`0${date.getMinutes()}`}`
            }
            return element
        }) 
        setContactList(editedContact)
        localStorage.setItem("contactList", JSON.stringify(editedContact));
        refForm.current.style.display = 'none';
    }

    const deleteContact = (index: number) => {
        const removedList = [...localStorageList].filter((element, index1) => index1 !== index);
        setContactList(removedList);
        localStorage.setItem("contactList", JSON.stringify(removedList));
    }
    const refNameInput:any = useRef<HTMLInputElement>(null);
    const refNumberInput:any = useRef<HTMLInputElement>(null);
    const refForm:any = useRef(null);


    return (
            <Container>
                <TableContainer>
                    <Box width='100' display='flex' justifyContent='space-between' alignItems='flex-start'>
                        <Button onClick={() => goToAdd()} color="primary" variant="contained">Create New</Button>
                        <form ref={refForm} className='edit-form' onSubmit={(e) => onEditSubmit(e)}>
                            <input ref={refNameInput}/>
                            <input ref={refNumberInput} type="text"/>
                            <button type='submit'>Edit</button>
                        </form>
                    </Box>
                    <Table aria-label="simple table">
                        <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell align="right">Number</TableCell>
                            <TableCell align="right">Edit</TableCell>
                            <TableCell align="right">Delete</TableCell>
                            <TableCell align="right">Created Date</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {localStorageList.map((contact:IContact, index:number) => (
                            <TableRow key={contact.id}>
                            <TableCell component="th" scope="row">
                                {contact.name}
                            </TableCell>
                            <TableCell align="right">{contact.number}</TableCell>
                            <TableCell align="right">
                                <Button onClick={() => editContact(index)} color="default" variant="contained">Edit</Button>
                            </TableCell>
                            <TableCell align="right">
                                <Button onClick={() => {deleteContact(index)}} color="secondary" variant="contained">Delete</Button>
                            </TableCell>
                            <TableCell align="right">{contact.createdDate}</TableCell>
                            </TableRow>
                        ))}
                        </TableBody>
                    </Table>
                    </TableContainer>
            </Container>
    );
}