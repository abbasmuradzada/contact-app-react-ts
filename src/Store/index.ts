import { createContext } from 'react';

export const UserContext = createContext<any>(null);
export const EditedContext = createContext<any>(null);

export interface IContact {
    id: number,
    name: string,
    number: number,
    createdDate: string
}

export let context: IContact[] = [
    {
        id: 1,
        name: 'Abbas Muradzada',
        number: 557580680,
        createdDate: '14:30'
    },
    {
        id: 2,
        name: 'Elmar Gasimov',
        number: 554544545,
        createdDate: '14:30'
    },
    {
        id: 3,
        name: 'Semyon Penziyev',
        number: 557585555,
        createdDate: '14:30'
    },
];